const { MoleculerError } = require("moleculer").Errors;

const _ = require('lodash')

module.exports = {
	methods: {
		async checkIsAuthenticated(ctx) {
			await this.broker.waitForServices({ name: "Session", version: 1 });
			try {
				const decoded = ctx.meta.auth;
				const session = await ctx.broker.call(
					"v1.SessionModel.findOne",
					[
						{
							id: decoded.sessionId,
							state: "ACTIVE"
						},
					]
				);
				if(_.get(session, "id", null) === null) throw new Error("Unauthenticated");
				const isMatch = ctx.meta.deviceId === session.deviceId;
				if (!isMatch) throw new Error("Unauthenticated");
				ctx.meta.session = decoded.sessionId;
			} catch (error) {
				throw new MoleculerError(
					`[Customer] Authenticate: ${error.message}`
				);
			}
		},
		checkUserRole(ctx) {
		},
		checkOwner(ctx) {
		},
	},
};
