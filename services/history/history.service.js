const _ = require("lodash");
const AuthMixin = require("../../mixins/authorize.mixin");

module.exports = {
	name: "History",
	mixins: [AuthMixin],

	version: 1,

	settings: {},

	actions: {
		createPayHistory: {
			params: {
				accountId: "number",
				transaction: "object",
			},

			handler: require("./actions/createPayOrderHistory.action"),
		},
	},

	created() {},

	async started() {},

	async stopped() {},
};
