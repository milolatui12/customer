const { MoleculerError } = require("moleculer").Errors;
const OrderConstant = require('../../order/constants/orderConstant')
const Numeral = require('numeral')
const _ = require("lodash");


module.exports = async function (ctx) {
	try {
		const { accountId, transaction } = ctx.params;

        

		const historyObj = {
            accountId,
            service: {
                type: "PAY ORDER",
                id: transaction.order.id,
                state: transaction.order.state,
                name: "Thanh toán order",
                data: {
                    balanceBefore: transaction.balanceBefore,
                    balanceAfter: transaction.balanceAfter,
                    fromWallet: transaction.fromWallet,
                    toWallet: transaction.toWallet,
                }
            },
            amount: transaction.order.amount,
            fee: transaction.order.fee,
            total: transaction.order.total,
            state: "PENDING",
            description: `Thanh toán ${Numeral(transaction.order.amount).format('0,0')} VND`
        }

        const history = await this.broker.call("v1.HistoryModel.create", [historyObj])

        if(_.get(history, "id", null) === null) {
            return {
                code: 1000,
                message: "Fail to create history",
            }; 
        }

        console.log(history);
		
        return history
	} catch (err) {
		if (err.name === "MoleculerError") throw err;
		throw new MoleculerError(
			`[History] Create history: ${err.message}`
		);
	}
};
