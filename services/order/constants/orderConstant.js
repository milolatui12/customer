module.exports = {
	STATE: {
		PENDING: 'PENDING',
		SUCCEEDED: 'SUCCEEDED',
		FAILED: 'FAILED',
		REJECTED: 'REJECTED',
	},
	PAYMENT_METHOD: {
		WALLET: 'WALLET',
		ATM_CARD: 'ATM_CARD'
	}
};