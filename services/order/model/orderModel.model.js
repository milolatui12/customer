const mongoose = require("mongoose");
const autoIncrement = require("mongoose-auto-increment");

const _ = require("lodash");
const OrderConstant = require("../constants/orderConstant");

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema(
	{
		id: {
			type: Number,
			required: true,
			unique: true,
		},
		accountId: {
			type: Number,
			require: true,
		},
		partnerAccountId: {
			type: Number,
			require: false,
		},
		amount: {
			type: Number,
			required: true,
		},
		fee: {
			type: Number,
			default: null,
		},
		total: {
			type: Number,
			default: 0,
		},
		state: {
			type: String,
			enum: _.values(OrderConstant.STATE),
		},
		description: {
			type: String,
			default: "",
		},
		notes: {
			type: String,
			default: "",
		},
		payment: {
			method: {
				type: String,
				enum: _.values(OrderConstant.PAYMENT_METHOD),
			},
			state: { type: String },
		},
	},
	{
		collection: "Order",
		versionKey: false,
		timestamps: true,
	}
);

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: "id",
	startAt: 1,
	incrementBy: 1,
});

module.exports = mongoose.model(Schema.options.collection, Schema);
