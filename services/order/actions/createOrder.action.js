const { MoleculerError } = require("moleculer").Errors;
// const OrderConstant = require("../../order/constants/orderConstant");
const Numeral = require("numeral");
const _ = require("lodash");

module.exports = async function (ctx) {
	try {
		const { amount, description, notes, paymentMethod } = ctx.params.body;
		const sessionId = ctx.meta.session;
		const { accountId } = await this.broker.call(
			"v1.SessionModel.findOne",
			[{ id: sessionId }]
		);
        const fee = 0.2
		const orderObj = {
            accountId,
            amount,
            fee,
            total: amount + (amount*fee),
            state: "PENDING",
            description,
            notes,
            payment: {
                method: paymentMethod,
                state: "PENDING"
            }
        };

        const order = await this.broker.call("v1.OrderModel.create", [
            orderObj
        ])
        if(_.get(order, "id", null) === null) {
            return {
                code: 1000,
                message: "Fail to create order",
            };
        }

        return {
			code: 1000,
			data: {
				message: "Create order successful",
				orderId: order.id
			},
		};
	} catch (err) {
		if (err.name === "MoleculerError") throw err;
		throw new MoleculerError(`[History] Create history: ${err.message}`);
	}
};
