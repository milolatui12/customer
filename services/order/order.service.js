const { MoleculerError } = require("moleculer").Errors;
const _ = require('lodash')

const QueueMixin = require("moleculer-rabbitmq")({
	connection: "amqp://localhost",
	asyncActions: true, // Enable auto generate .async version for actions
});

module.exports = {
	name: "Order",
	version: 1,

	mixins: [QueueMixin],

	settings: {
		amqp: {
			connection: "amqp://127.0.0.1", // You can also override setting from service setting
		},
	},
	actions: {
		create: {
			rest: {
				method: "POST",
				fullPath: "/v1/create-order",
				auth: {
					mode: "required",
				},
			},
			hooks: {
				before: ["checkIsAuthenticated"],
			},
			params: {
				body: {
					$$type: "object",
					amount: "number",
					description: "string",
					notes: "string",
					paymentMethod: "string",
				},
			},
			handler: require("./actions/createOrder.action"),
		},
	},
	methods: {
		async checkIsAuthenticated(ctx) {
			await this.broker.waitForServices({ name: "Session", version: 1 });
			try {
				const decoded = ctx.meta.auth;
				const session = await ctx.broker.call(
					"v1.SessionModel.findOne",
					[
						{
							id: decoded.sessionId,
							state: "ACTIVE",
						},
					]
				);
				if (_.get(session, "id", null) === null)
					throw new Error("Unauthenticated");
				const isMatch = ctx.meta.deviceId === session.deviceId;
				if (!isMatch) throw new Error("Unauthenticated");
				ctx.meta.session = decoded.sessionId;
			} catch (error) {
				throw new MoleculerError(
					`[Customer] Authenticate: ${error.message}`
				);
			}
		},
	},
};
