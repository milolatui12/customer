const { MoleculerError } = require("moleculer").Errors;

const QueueMixin = require("moleculer-rabbitmq")({
	connection: "amqp://localhost",
	asyncActions: true, // Enable auto generate .async version for actions
});

module.exports = {
	name: "Payment",
	version: 1,

	mixins: [QueueMixin],

	settings: {
		amqp: {
			connection: "amqp://127.0.0.1", // You can also override setting from service setting
		},
	},
	actions: {
		payWithWallet: {
			params: {
				accountId: "number",
				order: "object",
			},
			handler: require('./actions/payWithWallet.action')
		},
	},
	methods: {
		
	},
};
