const { MoleculerError } = require("moleculer").Errors;
const _ = require("lodash");

const OrderConstant = require("../../order/constants/orderConstant");

module.exports = async function (ctx) {
	try {
		const { accountId, order } = ctx.params;
		

		let unlock;
		let walletInfo = {};
		let partnerWalletInfo = {};
		try {
			unlock = await this.broker.cacher.lock(`wallet_${accountId}`);

			walletInfo = await this.broker.call("v1.UserWalletModel.findOne", [
				{ accountId },
			]);
			partnerWalletInfo = await this.broker.call(
				"v1.UserWalletModel.findOne",
				[
					{
						accountId: order.partnerAccountId,
					},
				]
			);

			if (_.get(walletInfo, "id", null) === null) {
				throw new Error("Fail")
			}

			if (walletInfo.balance < order.total) {
				throw new Error("Số dư không đủ")
			}

			const newBalance = walletInfo.balance - order.total;

			const fromWallet = await this.broker.call(
				"v1.UserWalletModel.findOneAndUpdate",
				[
					{
						accountId,
					},
					{
						balance: newBalance,
					},
				]
			);

			const transaction = {
				order: {
					...order,
				},
				balanceBefore: walletInfo.balance,
				balanceAfter: newBalance,
				fromWallet: fromWallet.id,
			};

			await this.broker.call("v1.History.createPayHistory", {
				accountId,
				transaction,
			});

			const newOrderObj = await this.broker.call(
				"v1.OrderModel.findOneAndUpdate",
				[
					{
						id: order.id,
						state: OrderConstant.STATE.PENDING,
					},
					{
						state: OrderConstant.STATE.SUCCEEDED,
					},
				]
			);
			if (_.get(newOrderObj, "id", null) === null) {
				throw new Error("Số dư không đủ")
			}
		} catch (err) {
			throw new MoleculerError(err);
		} finally {
			if (_.isFunction(unlock)) {
				await unlock();
			}
		}

		return {
			code: 1000,
			message: "Thành công",
		};
	} catch (err) {
		throw new MoleculerError(`[Paymen] Pay order: ${err.message}`);
	}
};
